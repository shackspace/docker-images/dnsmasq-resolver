FROM jpillora/dnsmasq:latest

RUN apk add ucarp

# copy start script
COPY entrypoint.sh /entrypoint.sh

# copy ifup ifdown scripts
COPY ucarp/vip-down-default.sh /vip-down-default.sh
COPY ucarp/vip-up-default.sh /vip-up-default.sh

# copy dnsmasq config into container
COPY dnsmasq.conf /etc/dnsmasq.conf

# disable ucarp in default
ENV UCARP_ENABLE=false

EXPOSE 53 53/udp
#VOLUME ["/etc/coredns"]
ENTRYPOINT ["/entrypoint.sh"]
